package com.weather.api.service;



import com.weather.api.models.repositories.WeatherRecord;
import com.weather.api.models.rest.Response;
import com.weather.api.models.rest.WeatherData;

import java.util.List;

public interface WeatherService {

  public Response getWeatherByCity(String city);
  public boolean saveWeatherData(WeatherData weatherData);
  public List<WeatherData> getWeatherDatasByCity(String city);
  public boolean removeWeatherData(String weatherId);
  public WeatherRecord updateWeatherData(String weatherId, WeatherData weatherData);

  public boolean checkWeatherIdExist(String weatherId);
}
