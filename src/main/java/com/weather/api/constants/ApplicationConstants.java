package com.weather.api.constants;

public class ApplicationConstants {
  public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
}
